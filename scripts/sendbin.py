import serial
import struct
import sys
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-i", "--input", dest="inputfile",
                  help="read binary data from FILE", metavar="FILE")
parser.add_option("-a", "--address", dest="address", type="int",
                  help="destination address ADDR", metavar="ADDR")
parser.add_option("-e", "--exec", dest="execaddr", type="int",
                  help="execute at address ADDR", metavar="ADDR")
(options, args) = parser.parse_args()

if options.inputfile is not None and options.address is None:
    print "Address must be specified for input file"
    sys.exit(1)
if options.inputfile is None and options.execaddr is None:
    print "Specify either an input file, an execute address, or both"
    sys.exit(1)

print "Opening serial port"
ser = serial.Serial(0, 19200, timeout=60, rtscts=0)
if options.inputfile is not None:
    f = open(options.inputfile, 'rb')
    bindata = f.read()
    f.close()
    hdr = struct.pack(">BLL", 1, options.address, len(bindata))
    hdrdump = ""
    for c in hdr:
        hdrdump += hex(ord(c)) + " "
    print "Sending header - " + hdrdump
    ser.write(hdr)
    print "Sending data"
    ser.write(bindata)
if options.execaddr is not None:
    print "Sending execute instruction"
    execcmd = struct.pack(">BL", 2, options.execaddr)
    ser.write(execcmd)
ser.close()
print "Sent."
