import serial
import sys
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-i", "--input", dest="inputfile",
                  help="read ADF data from FILE", metavar="FILE")
(options, args) = parser.parse_args()

if options.inputfile is None:
    print "Input file must be specified"
    sys.exit(1)


print "Opening serial port"
ser = serial.Serial(0, 19200, timeout=60, rtscts=1)
f = open(options.inputfile, 'rb')
adfdata = f.read()
f.close()
print "Sending data"
ser.write(adfdata)
ser.close()
print "Sent."
