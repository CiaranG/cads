

RunAddress	= 0x7FC00

CUSTOM	= 0xDFF000	 |Base address for all custom chip registers

INTENA	= 0x9A
INTREQ	= 0x9C
INTREQR	= 0x1E

DMACON	= 0x96

COLOR00	= 0x180

SERDAT	= 0x30
SERDATR	= 0x18
SERPER	= 0x32

INTVEC5 = 0x60+4*5		|Vector for level 5 interrupt

Start:	dc.b	'D','O','S',0
	dc.l	0		|Checksum will go here
	dc.l	880
	
	#We'll keep a6 pointing to the custom chip registers throughout...
	lea	CUSTOM,a6

	#Disable all interrupts and requests
	move.w	#0x7FFF,INTENA(a6)
	move.w	#0x7FFF,INTREQ(a6)

	#Disable all DMA
	move.w	#0x7FFF,DMACON(a6)

	move.w	#0xF0F,COLOR00(a6)	| Pink background

	#Commit a privelege violation to get into supervisor mode...
	lea	Super(pc),a0
	move.l	a0,0x20
Super:	move.w	#0x2000,sr

	#Set up stack - note that we only use 12 bytes of stack space
	lea	0x80000,sp

	#Relocate our main resident code to the correct address...
	lea	Start(pc),a0		|The address we got loaded at
	move.l	#RunAddress,a1		|The real start address!
	move.l	#ResidentEnd-Start-1,d0
RelocLp:	move.b	(a0)+,(a1)+
	dbf	d0,RelocLp

	move.w	#0x0F0,COLOR00(a6)	| Green background

	#Jump to the newly relocated code...
	jmp	RunAddress+(Resident-Start)

Resident:
	move.w	#0x000,COLOR00(a6)	| Black background

	#Set vector and enable interrupts to handle serial receive
	#buffer full...
	sub.l	d0,d0
	move.l	d0,ExecAddress
	move.b	d0,ReceiveState
	move.w	#0xB8,SERPER(a6)	|19200 baud (maybe 0xB9 for NTSC)
	lea	IntLevel5(pc),a0
	move.l	a0,INTVEC5
	move.w	#0xC800,INTENA(a6)

	#Sit in a loop unless we're told to execute something...
	#Note that the interrupt handler expects a6 to be pointing at the
	#custom chips, and may corrupt anything other register!!
ILoop:	tst.l	ExecAddress
	beq.s	ILoop
	move.w	#0x07FFF,INTENA(a6)	|Interrupts off
	move.w	#2700,sr
	move.w	#0x0F0,COLOR00(a6)	|Green background
	move.l	ExecAddress,a0
	jmp	(a0)

ExecAddress:	dc.l	0	|When this becomes non-zero, the loader is
				|finished and we should execute the target
				|code at the given address.
ReceivePtr:	dc.l	0
ReceiveCount:	dc.l	0
ReceiveBuffer:	dc.l	0,0
ReceivePendCmd:	dc.b	0
ReceiveState:	dc.b	0

		#Serial interrupt handler - we expect a6 to always be pointing
		#at the custom chips, and can corrupt any other registers. This
		#strange arrangement allows us to bail out of the handler at
		#any point with just an rte.
IntLevel5:	#We don't have to do any checking here, since we know there
		#is only one possible interrupt this could be, so we just need
		#to clear the request...
		move.w	#0x0800,INTREQ(a6)

		#Read the byte received...
		move.w	SERDATR(a6),d0

		sub.l	d1,d1
		move.b	ReceiveState,d1
		dbra	d1,NotS0
		#State 0 = waiting for a command byte, 1-n
State0:		move.b	d0,ReceivePendCmd
		move.l	#ReceiveBuffer,ReceivePtr
		move.b	#1,ReceiveState
		sub.b	#1,d0
		beq.s	Cmd1
		sub.b	#1,d0
		beq.s	Cmd2
		move.b	#2,ReceiveState
		bra	Error		|Invalid command!
Cmd2:		move.l	#4-1,ReceiveCount
		rte
Cmd1:		move.l	#8-1,ReceiveCount
		rte

NotS0:		dbra	d1,Error
		#State 1 = Receiving ReceiveCount+1 bytes of data and storing
		#          at ReceivePtr. If ReceivePendCmd > 0 then when
		#          all the bytes have been received, that command
		#          should be processed using the 'parameters' at
		#	   ReceiveBuffer.
State1:		move.l	ReceivePtr,a0
		move.l	ReceiveCount,d1
		move.b	d0,(a0)+
		subq.l	#1,d1
		bmi	S1Done
		move.l	a0,ReceivePtr
		move.l	d1,ReceiveCount
		rte

FinishS1:	move.b	#0,ReceiveState
		rte

S1Done:		sub.l	d1,d1
		move.b	ReceivePendCmd,d1
		dbra	d1,Not0
		bra.s	FinishS1
Not0:		dbra	d1,Not1
		#Cmd 1 = Receive n bytes of data to address x
		move.l	ReceiveBuffer+4,d0
		subq.l	#1,d0
		move.l	d0,ReceiveCount
		move.l	ReceiveBuffer,d0
		move.l	d0,ReceivePtr
		move.b	#0,ReceivePendCmd
		#We stay in state 1 to receive the actual data
		rte

Not1:		dbra	d1,Error
		#Cmd2 = Execute at address x
ExecCmd2:	move.l	ReceiveBuffer,d0
		move.l	d0,ExecAddress
		bra	FinishS1

Error:		move.b	#2,ReceiveState
		#State 2 (or anything else undefined)  = It's all gone wrong
		#so set a red background...
		move.w	#0xF00,COLOR00(a6)
		rte

ResidentEnd:


