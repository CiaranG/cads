
CUSTOM	= 0xDFF000	 |Base address for all custom chip registers

BPLCON0	= 0x100
BPLCON1	= 0x102
BPLCON2	= 0x104

INTENA	= 0x9A
INTREQ	= 0x9C
INTREQR	= 0x1E

COPJMP1	= 0x88
COP1LCH	= 0x80

DMACON	= 0x96
SETCLR	= 0x8000
COPEN	= 0x80

BPL1PTH	= 0xE0
BPL1PTL	= 0xE2
BPL2PTH	= 0xE4
BPL2PTL	= 0xE6
BPL3PTH	= 0xE8
BPL3PTL	= 0xEA
BPL4PTH	= 0xEC
BPL4PTL	= 0xEE
BPL5PTH	= 0xF0
BPL5PTL	= 0xF2
BPL6PTH	= 0xF4
BPL6PTL	= 0xF6

BPL1MOD	= 0x108
BPL2MOD = 0x10A

COLOR00	= 0x180
COLOR01	= 0x182
COLOR02	= 0x184
COLOR03	= 0x186

DDFSTRT	= 0x92
DDFSTOP	= 0x94
DIWSTRT	= 0x8E
DIWSTOP	= 0x90

SERDAT	= 0x30
SERDATR	= 0x18
SERPER	= 0x32

INTVEC5 = 0x60+4*5		|Vector for level 5 interrupt

CopList		= 0x20000		|We'll put our copper list here
Screen		= 0x21000		|We'll put our screen here
ScreenWidth	= 320
ScreenHeight	= 200

	#Startup - we expect to be in supervisor mode, with all interrupts
	#          disabled.
Startup:
	#We'll keep a6 pointing to the custom chip registers throughout...
	lea	CUSTOM,a6

	#Disable all DMA
	move.w	#0x7FFF,DMACON(a6)

	#Set up stack
	lea	0x80000,sp

	#Set up the screen geometry...
	move.w	#0x1200,BPLCON0(a6)
	move.w	#0,BPLCON1(a6)
	move.w	#0,BPL1MOD(a6)
	move.w	#0x38,DDFSTRT(a6)
	move.w	#0xD0,DDFSTOP(a6)
	move.w	#0x2C81,DIWSTRT(a6)
	move.w	#0xF4C1,DIWSTOP(a6)

	#Set up some colours...
	move.w	#0x000,COLOR00(a6)
	move.w	#0x0F0,COLOR01(a6)

	#Fill the screen with some data...
	lea	Screen,a0
	move.l	#0xF500F500,d0
	move.w	#ScreenWidth/32*ScreenHeight-1,d1
Loop:	move.l	d0,(a0)+
	dbf	d1,Loop

	#Generate our copper list...
	lea	CopList,a0
	move.l	a0,COP1LCH(a6)
	#Set address of bitplane 1...
	move.w	#BPL1PTH,(a0)+
	move.w	#(Screen>>16),(a0)+
	move.w	#BPL1PTL,(a0)+
	move.w	#(Screen&0xFFFF),(a0)+
	#Infinite wait stops copper until next vblank...
	move.l	#0xFFFFFFFE,(a0)+

	#Start the copper (the value written is irrelevant)...
	move.w	d0,COPJMP1(a6)

	#Enable copper and bitplane DMA...
	move.w	#0x8380,DMACON(a6)

	#Sit in a loop...
ILoop:
	bra.s	ILoop


